import { NgModule } from '@angular/core';
import {TableModule} from 'primeng/table';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DialogModule } from 'primeng/dialog';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {FieldsetModule} from 'primeng/fieldset';
import {PanelMenuModule} from 'primeng/panelmenu';
import {TooltipModule} from 'primeng/tooltip';
import {CheckboxModule} from 'primeng/checkbox';

@NgModule({
  declarations: [],
  exports: [
    TableModule,
    PanelMenuModule,
    ProgressSpinnerModule,
    DialogModule,
    AccordionModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    TooltipModule,
    FieldsetModule,
    CheckboxModule
],
providers: [],
})
export class AppPrimengModule { }
