import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/models/persona.model';
import { GestionPersonaService } from 'src/app/services/gestion-persona.service';

@Component({
  selector: 'app-gestion-persona',
  templateUrl: './gestion-persona.component.html',
  styleUrls: ['./gestion-persona.component.css']
})
export class GestionPersonaComponent implements OnInit {

  constructor(private gestionPersonaService: GestionPersonaService) { }

  ngOnInit(){
    this.obtenerPersona();
    this.obtenerPersonaPorId();
    this.guardar();
  }

  obtenerPersona(){
    this.gestionPersonaService.obtenerTodasPersonas().subscribe(
      result => {
        console.log("RESULTADO SW",result);
      },
      error => {
        console.log("RESULTADO error",error);
      },
      () => {
        console.log('Completado');
      }
    );
  }

  obtenerPersonaPorId(){
    this.gestionPersonaService.obtenerPersonaPorId(2).subscribe(
      result => {
        console.log("RESULTADO POR ID SW:",result);
      },
      error => {
        console.log('Hubo errores ==>',error);
      },
      () => {
        console.log('Completado');
      }
    );
  }

  guardar() {
    let persona: Persona = new Persona();

    persona.dni = '55555555';
    persona.nombre = 'Jesus';
    persona.apellido = 'Herrera';
    persona.edad = 10;

    this.gestionPersonaService.insertarPersona(persona).subscribe(
      result => {
        console.log('RESULTADO POR ID SW: ', result);
      },
      error => {
        console.log('Hubo errores ==>', error);
      },
      () => {
        console.log('Completado');
      }
    );

  }


}
