export class Persona {
  id?:number;
  dni?:string;
  nombre?:string;
  apellido?:string;
  edad?:number;
  sexo?:string;
  fechaNacimiento?:string;
  celular?:string;
  gustos?:string;
  notas?:number;
  cargos?:string;
}
