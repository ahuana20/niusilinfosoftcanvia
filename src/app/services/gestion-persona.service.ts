import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Persona } from '../models/persona.model';

@Injectable({
  providedIn: 'root'
})
export class GestionPersonaService {

  //URI_GESTION_PERSONA = 'http://localhost:8084';

  constructor(private http: HttpClient) { }

  obtenerTodasPersonas():Observable<Persona[]> {
    return this.http.get<Persona[]>(`${environment.urlBase}/personas`);
  }

  obtenerPersonaPorId(id :number):Observable<Persona> {
    return this.http.get<Persona>(`${environment.urlBase}/personas/${id}`);
  }

  buscarPersonas1(descripcion: string): Observable<Persona[]> {
    return this.http.get<Persona[]>(`${environment.urlBase}/personas/buscar1/${descripcion}`);
  }

  buscarPersonas2(descripcion: string): Observable<Persona[]> {
    return this.http.get<Persona[]>(`${environment.urlBase}/personas/buscar2/${descripcion}`);
  }

  insertarPersona(persona:Persona): Observable<Persona> {
    const parametro: string = JSON.stringify(persona);
    let myHeader = new HttpHeaders();
    myHeader = myHeader.set('Content-Type', 'application/json');
    return this.http.post<Persona>(`${environment.urlBase}/personas`, parametro, {headers: myHeader});
  }

  actualizarPersona(persona: Persona): Observable<Persona> {
    const parametro: string = JSON.stringify(persona);
    let myHeader: HttpHeaders = new HttpHeaders();
    myHeader = myHeader.set('Content-Type', 'application/json');
    return this.http.put<Persona>(`${environment.urlBase}/personas/${persona.id}`, parametro, { headers: myHeader });
  }

  eliminarPersona(id: number): Observable<Persona> {
    return this.http.delete<Persona>(`${environment.urlBase}/personas/${id}`);
  }

}
